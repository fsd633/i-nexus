declare var google: any;

import { Component, inject } from '@angular/core';
import { LoginComponent } from '../login/login.component';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrl: './browse.component.css'
})
export class BrowseComponent {
 auth=inject(AuthService);
 name= JSON.parse(sessionStorage.getItem("LoggedInUser")!).name;
 userProfileImg= JSON.parse(sessionStorage.getItem("LoggedInUser")!).picture;
 email= JSON.parse(sessionStorage.getItem("LoggedInUser")!).email;

 signOut(){
  sessionStorage.removeItem("LoggedInUser");
  this.auth.signOut();
 }

}