import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ProductsComponent } from './products/products.component';
import { CategoriesComponent } from './categories/categories.component';
import { CartComponent } from './cart/cart.component';
import { authGuard } from './auth.guard';
import { HomeComponent } from './home/home.component';
import { LogoutComponent } from './logout/logout.component';
import { ContactusComponent } from './contactus/contactus.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { PhonesComponent } from './phones/phones.component';
import { LaptopsComponent } from './laptops/laptops.component';
import { TvsComponent } from './tvs/tvs.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginadminComponent } from './loginadmin/loginadmin.component';
import { HostComponent } from './host/host.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { ShowAllUsersComponent } from './show-all-users/show-all-users.component';
import { GetUserByEmailComponent } from './get-user-by-email/get-user-by-email.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'contactus', component: ContactusComponent },
  { path: 'categories', canActivate: [authGuard], component: CategoriesComponent },
  { path: 'products', canActivate: [authGuard], component: ProductsComponent },
  { path: 'cart', canActivate: [authGuard], component: CartComponent },
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'wishlist', component: WishlistComponent },
  { path: 'loginadmin', component: LoginadminComponent },
  { path: 'host', component: HostComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'admin-dashboard', component: AdminDashboardComponent },
  { path: 'show-all-users', component: ShowAllUsersComponent }, // Add route for ShowAllUsersComponent
  { path: '', redirectTo: '/register', pathMatch: 'full' },
  { path: 'get-user-by-email', component: GetUserByEmailComponent },


  { path: 'logout', component: LoginComponent },
  { path: '', redirectTo: '/categories', pathMatch: 'full' },
  { path: 'categories', component: CategoriesComponent },
  { path: 'categories/phones', component: PhonesComponent },
  { path: 'categories/laptops', component: LaptopsComponent },
  { path: 'categories/tvs', component: TvsComponent },
  { path: '**', redirectTo: '/categories', pathMatch: 'full' },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'reset-password', component: ResetPasswordComponent }
];




@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
