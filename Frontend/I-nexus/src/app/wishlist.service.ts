// wishlist.service.ts

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {
  private wishlistItems: any[] = []; // Assuming this holds the wishlist items

  constructor() {}

  getWishlistItems(): any[] {
    return this.wishlistItems;
  }

  addToWishlist(item: any): void {
    // Check if item is already in wishlist
    const existingItem = this.wishlistItems.find(i => i.id === item.id);
    if (!existingItem) {
      this.wishlistItems.push(item);
    }
  }

  removeFromWishlist(item: any): void {
    const index = this.wishlistItems.findIndex(i => i.id === item.id);
    if (index !== -1) {
      this.wishlistItems.splice(index, 1);
    }
  }

  addToCart(item: any): void {
    // Add logic to move item to cart
    console.log('Added to cart:', item);
  }

  getTotalPrice(): number {
    let totalPrice = 0;
    for (const item of this.wishlistItems) {
      totalPrice += item.price;
    }
    return totalPrice;
  }
}
