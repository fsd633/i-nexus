// cart.component.ts

import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartItems: any[] = [];

  constructor(private cartService: CartService) {}

  ngOnInit(): void {
    this.cartItems = this.cartService.getCartItems();
  }

  removeFromCart(item: any) {
    this.cartService.removeFromCart(item);
    this.cartItems = this.cartService.getCartItems();
  }

  moveToWishlist(item: any) {
    this.cartService.moveToWishlist(item); // Call moveToWishlist from CartService
    // Optionally, update cartItems if needed
    this.cartItems = this.cartService.getCartItems();
  }

  increaseQuantity(item: any) {
    this.cartService.increaseQuantity(item);
    // Optionally, update cartItems if needed
    this.cartItems = this.cartService.getCartItems();
  }

  decreaseQuantity(item: any) {
    this.cartService.decreaseQuantity(item);
    // Optionally, update cartItems if needed
    this.cartItems = this.cartService.getCartItems();
  }

  getTotalPrice() {
    return this.cartService.getTotalPrice();
  }
}
