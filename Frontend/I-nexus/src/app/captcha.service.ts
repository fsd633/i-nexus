import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CaptchaService {
  refreshCaptcha() {
    throw new Error('Method not implemented.');
  }
  constructor() { }

  generateCaptcha(): string {
    let value = btoa((Math.random() * 1000000000).toString());
    value = value.substring(0, 5 + Math.floor(Math.random() * 5));
    return value;
  }
}
