import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-show-all-users',
  templateUrl: './show-all-users.component.html',
  styleUrls: ['./show-all-users.component.css']
})
export class ShowAllUsersComponent implements OnInit {

  users: any[] = []; // Define users array to hold fetched users

  constructor(private adminService: AdminService) {}

  ngOnInit(): void {
    this.getAllUsers();
  }

  getAllUsers() {
    this.adminService.getAllUsers().subscribe(
      (data: any) => {
        this.users = data; // Assign fetched users to local array
      },
      (error: any) => {
        console.error('Error fetching users:', error); // Handle error if any
      }
    );
  }
  
}
