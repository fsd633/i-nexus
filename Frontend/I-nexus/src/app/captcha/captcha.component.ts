import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-captcha',
  templateUrl: './captcha.component.html',
  styleUrls: ['./captcha.component.css']
})
export class CaptchaComponent implements OnInit {
  captchaValue: string = '';
  inputCaptchaValue: string = '';
  captchaHTML: string = '';
  fonts: string[] = ['Arial', 'Verdana', 'Tahoma', 'Georgia', 'Courier'];

  constructor() { }

  ngOnInit(): void {
    this.generateCaptcha();
    this.setCaptcha();
  }

  generateCaptcha() {
    let value = btoa((Math.random() * 1000000000).toString());
    value = value.substring(0, 5 + Math.floor(Math.random() * 5)); // Ensure substring end is an integer
    this.captchaValue = value;
  }

  setCaptcha() {
    this.captchaHTML = this.captchaValue.split("").map((char) => {
      const rotate = 20 + Math.floor(Math.random() * 30);
      const font = this.fonts[Math.floor(Math.random() * this.fonts.length)];
      return `<span style="display: inline-block; transform: rotate(${rotate}deg); font-family: ${font};">${char}</span>`;
    }).join("");
  }

  verifyCaptcha(): boolean {
    return this.inputCaptchaValue === this.captchaValue;
  }
}
