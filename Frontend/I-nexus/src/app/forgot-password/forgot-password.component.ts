import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ForgotPasswordService } from '../forgot-password.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  forgotPasswordForm: FormGroup;
  message: string | undefined;

  constructor(
    private fb: FormBuilder,
    private forgotPasswordService: ForgotPasswordService
  ) {
    this.forgotPasswordForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  get email() {
    return this.forgotPasswordForm.get('email')!;
  }

  onSubmit() {
    if (this.forgotPasswordForm.valid) {
      this.forgotPasswordService.sendResetLink(this.forgotPasswordForm.value.email).subscribe({
        next: (response) => {
          this.message = 'A password reset link has been sent to your email.';
        },
        error: (error) => {
          this.message = 'There was an error sending the reset link. Please try again.';
        }
      });
    }
  }
}
