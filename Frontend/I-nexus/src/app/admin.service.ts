import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private baseUrl = 'http://localhost:8085'; // Replace with your Spring Boot backend URL

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}/getAllUsers`);
  }

  getUserById(userId: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/getUserById/${userId}`);
  }
  getUserByEmail(email: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/users/email/${email}`);
  }

  // Add more methods for other admin functionalities like updating user details, deleting users, etc.
}
