import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-get-user-by-email',
  templateUrl: './get-user-by-email.component.html',
  styleUrls: ['./get-user-by-email.component.css']
})
export class GetUserByEmailComponent implements OnInit {

  email: string = '';
  user: any;
  errorMessage: string = '';

  constructor(private adminService: AdminService) {}

  ngOnInit(): void {}

  getUserByEmail() {
    this.adminService.getUserByEmail(this.email).subscribe(
      (data: any) => {
        if (data) {
          this.user = data;
          this.errorMessage = '';
        } else {
          this.user = null;
          this.errorMessage = 'User not found.';
        }
      },
      (error: any) => {
        console.error('Error fetching user:', error);
        this.errorMessage = 'Error fetching user.';
      }
    );
  }
}
