// cart.service.ts

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private cartItems: any[] = [];

  constructor() { }

  getCartItems(): any[] {
    return this.cartItems;
  }

  addToCart(item: any): void {
    // Add item to cart logic
    this.cartItems.push(item);
  }

  removeFromCart(item: any): void {
    // Remove item from cart logic
    this.cartItems = this.cartItems.filter(cartItem => cartItem !== item);
  }

  moveToWishlist(item: any): void {
    // Move item to wishlist logic
    // Implement how you want to handle moving items to wishlist
    console.log('Moving item to wishlist:', item);
    // Example: Remove item from cart, add to wishlist array, etc.
  }

  increaseQuantity(item: any): void {
    // Increase item quantity logic
    item.quantity++;
  }

  decreaseQuantity(item: any): void {
    // Decrease item quantity logic
    if (item.quantity > 1) {
      item.quantity--;
    }
  }

  getTotalPrice(): number {
    // Calculate total price logic
    let totalPrice = 0;
    this.cartItems.forEach(item => {
      totalPrice += (item.price * item.quantity);
    });
    return totalPrice;
  }
}
