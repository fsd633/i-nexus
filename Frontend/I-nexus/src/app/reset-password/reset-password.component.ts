import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  otp: string = '';
  newPassword: string = '';
  email: string = '';

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.email = params['email'];
    });
  }

  resetPassword() {
    const payload = {
      email: this.email,
      otp: this.otp,
      newPassword: this.newPassword
    };

    this.http.post('http://localhost:8888/verifyOtpAndResetPassword', payload)
      .subscribe(
        (response: any) => {
          alert('Password reset successfully.');
          this.router.navigate(['/login']);
        },
        (error: any) => {
          alert('Error resetting password. Please check the OTP and try again.');
        }
      );
  }
}
