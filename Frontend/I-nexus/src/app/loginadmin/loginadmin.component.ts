import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-loginadmin',
  templateUrl: './loginadmin.component.html',
  styleUrls: ['./loginadmin.component.css']
})
export class LoginadminComponent implements OnInit {
  
  users: any;
  user: any;
  
  constructor(private router: Router, private toastr: ToastrService) {
    this.users = [
      { id: 1, user_name: 'Tharun', country: 'IND', phone_number: '9390372067', email_id: 'tahrungundela@gmail.com', password: 'Dhoni777@' },
    ];
  }

  ngOnInit() {
  }

  loginSubmit(loginForm: any) {
    console.log(loginForm);
    
    if (loginForm.emailId == 'INX' && loginForm.password == 'INX') {
      this.toastr.success('Login Success', 'Success');
      this.router.navigate(['/admin-dashboard']);
    } else {
      this.user = null;
      const foundUser = this.users.find((element: any) => element.email_id === loginForm.emailId && element.password === loginForm.password);

      if (foundUser) {
        this.toastr.success('Login Success', 'Success');
        this.router.navigate(['/admin-dashboard']);
      } else {
        this.toastr.error('Invalid Credentials', 'Error');
      }
    }
  }
}
