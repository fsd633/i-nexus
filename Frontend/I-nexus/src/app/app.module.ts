import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule} from '@angular/common/http'; // Ensure this import is correct

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CartComponent } from './cart/cart.component';
import { RegisterComponent } from './register/register.component';
import { ProductsComponent } from './products/products.component';
import { CategoriesComponent } from './categories/categories.component';
import { UserService } from './user.service';
import { HomeComponent } from './home/home.component';
import { LogoutComponent } from './logout/logout.component';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { ContactusComponent } from './contactus/contactus.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { PhonesComponent } from './phones/phones.component';
import { LaptopsComponent } from './laptops/laptops.component';
import { TvsComponent } from './tvs/tvs.component';
import { CartService } from './cart.service';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { CaptchaComponent } from './captcha/captcha.component';
import { CaptchaService } from './captcha.service';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { RouterModule } from '@angular/router';
import { BrowseComponent } from './browse/browse.component';
import { LoginadminComponent } from './loginadmin/loginadmin.component';
import { HostComponent } from './host/host.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { ShowAllUsersComponent } from './show-all-users/show-all-users.component';
import { GetUserByEmailComponent } from './get-user-by-email/get-user-by-email.component';
import { AdminService } from './admin.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AboutusComponent,
    LoginComponent,
    CartComponent,
    RegisterComponent,
    ProductsComponent,
    CategoriesComponent,
    HomeComponent,
    LogoutComponent,
    FooterComponent,
    ContactusComponent,
    WishlistComponent,
    PhonesComponent,
    LaptopsComponent,
    TvsComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    CaptchaComponent,
    BrowseComponent,
    
    ForgotPasswordComponent,
          LoginadminComponent,
          HostComponent,
          AdminDashboardComponent,
          ShowAllUsersComponent,
          GetUserByEmailComponent,
    
    


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([]),
    RecaptchaModule,
    RecaptchaFormsModule
  ],
  providers: [UserService,CartService,CaptchaService,AdminService],

  bootstrap: [AppComponent]
})
export class AppModule { }