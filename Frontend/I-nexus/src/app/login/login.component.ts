declare var google:any;
import { Component, OnInit, inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit(): void {

    google.accounts.id.initialize({
      client_id:"561849654401-rrkkigpshqgc5fbhnmj9h7domhu6uoac.apps.googleusercontent.com",
      callback:(resp:any)=>this.handleLogin(resp)
    });
    google.accounts.id.renderButton(document.getElementById("google-btn"),{
      theme:'filled_blue',
      size:'large',
      shape:'rectangle',
      width:350
    })
  }
  private deocdeToken(token:string){
    return JSON.parse(atob(token.split(".")[1]));
  }
  handleLogin(response:any){
    if(response){
      const payLoad = this.deocdeToken(response.credential);
      sessionStorage.setItem("LoggedInUser", JSON.stringify(payLoad));
      this.router.navigate(['browse'])

    }

  }

  async loginSubmit(loginForm: NgForm): Promise<void> {
    if (loginForm.valid) {
      const user = {
        emailId: loginForm.value.emailId,
        password: loginForm.value.password
      };

      try {
        const response: any = await this.userService.userLogin(user);

        if (response && response.status === 'success') {
          this.userService.setIsUserLoggedIn();
          this.router.navigate(['']);
          this.toastr.success('Login successful!', 'Success');
        } else {
          this.toastr.error('Invalid username or password.', 'Error');
        }
      } catch (error: any) {
        console.error('Login Error:', error);
        if (error.status === 401) {
          this.toastr.error('Invalid username or password.', 'Error');
        } else {
          this.toastr.error('An error occurred during login.', 'Error');
        }
      }
    } else {
      this.toastr.error('Please enter valid credentials.', 'Error');
    }
  }
}