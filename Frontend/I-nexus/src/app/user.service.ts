import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private apiUrl = 'http://localhost:8085';

  private isUserLoggedIn: boolean;
  public loginStatus: Subject<boolean>;

  constructor(private http: HttpClient, private toastr: ToastrService) {
    this.isUserLoggedIn = false;
    this.loginStatus = new Subject<boolean>();
  }

  resetPassword(email: string): Observable<any> {
    return this.http.post(`${this.apiUrl}/forgot-password`, { email });
  }

  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    localStorage.setItem('isUserLoggedIn', 'true');
    this.loginStatus.next(true);
  }

  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
    localStorage.removeItem('isUserLoggedIn');
    this.loginStatus.next(false);
  }

  getIsUserLoggedIn(): boolean {
    return localStorage.getItem('isUserLoggedIn') === 'true';
  }

  getUserLoginStatus(): Observable<boolean> {
    return this.loginStatus.asObservable();
  }

  userLogin(user: any): Promise<any> {
    console.log('Sending login request:', user); // Log request data
    return this.http.post(`${this.apiUrl}/userLogin`, user).toPromise();
  }

  getAllCountries(): Observable<any> {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  registerUser(user: any): Observable<any> {
    return this.http.post(`${this.apiUrl}/registerUser`, user);
  }
}
