// wishlist.component.ts

import { Component, OnInit } from '@angular/core';
import { WishlistService } from '../wishlist.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {
  wishlistItems: any[] = [];
  totalPrice: number = 0;

  constructor(private wishlistService: WishlistService) {}

  ngOnInit(): void {
    this.wishlistItems = this.wishlistService.getWishlistItems();
    this.calculateTotalPrice();
  }

  removeFromWishlist(item: any) {
    this.wishlistService.removeFromWishlist(item);
    this.wishlistItems = this.wishlistService.getWishlistItems();
    this.calculateTotalPrice();
  }

  addToCart(item: any) {
    this.wishlistService.addToCart(item);
    this.removeFromWishlist(item);
    this.wishlistItems = this.wishlistService.getWishlistItems();
    this.calculateTotalPrice();
  }

  calculateTotalPrice() {
    this.totalPrice = this.wishlistService.getTotalPrice();
  }
}
