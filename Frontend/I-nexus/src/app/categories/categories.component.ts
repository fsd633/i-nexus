// categories.component.ts

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categories: { name: string, route: string }[] = [
    { name: 'Phones', route: '/categories/phones' },
    { name: 'Laptops', route: '/categories/laptops' },
    { name: 'TVs', route: '/categories/tvs' }
  ];

  constructor(private router: Router) {}

  ngOnInit(): void {
    // Initialization logic if needed
  }

  navigateToCategory(route: string) {
    this.router.navigate([route]);
  }
}
