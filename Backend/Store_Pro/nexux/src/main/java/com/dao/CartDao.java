package com.dao;

import com.model.CartItem;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

public interface CartDao {

	@Transactional
    void addToCart(CartItem cartItem);

    List<CartItem> getAllCartItems();

    List<CartItem> getCartItemsByUserId(Long userId);

    List<CartItem> getCartItemsByUserEmail(String userEmail);

    void removeFromCart(Long cartItemId);

    void updateCartItem(Long cartItemId, CartItem cartItem);
}
