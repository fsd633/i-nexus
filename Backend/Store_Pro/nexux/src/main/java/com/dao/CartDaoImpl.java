package com.dao;

import com.model.CartItem;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public  class CartDaoImpl implements CartDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void addToCart(CartItem cartItem) {
        entityManager.persist(cartItem);
    }

    @Override
    public List<CartItem> getCartItemsByUserId(Long userId) {
        String query = "SELECT c FROM CartItem c WHERE c.userId = :userId";
        return entityManager.createQuery(query, CartItem.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<CartItem> getCartItemsByUserEmail(String userEmail) {
        String query = "SELECT c FROM CartItem c WHERE c.userEmail = :userEmail";
        return entityManager.createQuery(query, CartItem.class)
                .setParameter("userEmail", userEmail)
                .getResultList();
    }

    @Override
    public void removeFromCart(Long cartItemId) {
        CartItem cartItem = entityManager.find(CartItem.class, cartItemId);
        if (cartItem != null) {
            entityManager.remove(cartItem);
        }
    }

	@Override
	public List<CartItem> getAllCartItems() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateCartItem(Long cartItemId, CartItem cartItem) {
		// TODO Auto-generated method stub
		
	}

}
