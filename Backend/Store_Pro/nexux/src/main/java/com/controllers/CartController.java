package com.controllers;

import com.dao.CartDao;
import com.model.CartItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CartController {

    @Autowired
    private CartDao cartDao;

    // Endpoint to add an item to the cart
    @Transactional
    @PostMapping("/add")
    public ResponseEntity<Void> addToCart(@RequestBody CartItem cartItem) {
        cartDao.addToCart(cartItem);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    // Endpoint to get all cart items
    @GetMapping("/items")
    public List<CartItem> getAllCartItems() {
        return cartDao.getAllCartItems();
    }

    // Endpoint to get cart items by user ID
    @GetMapping("/items/user/{userId}")
    public List<CartItem> getCartItemsByUserId(@PathVariable Long userId) {
        return cartDao.getCartItemsByUserId(userId);
    }

    // Endpoint to get cart items by user email
    @GetMapping("/items/email")
    public List<CartItem> getCartItemsByUserEmail(@RequestParam String userEmail) {
        return cartDao.getCartItemsByUserEmail(userEmail);
    }

    // Endpoint to remove an item from the cart
    @DeleteMapping("/remove/{cartItemId}")
    public ResponseEntity<Void> removeFromCart(@PathVariable Long cartItemId) {
        cartDao.removeFromCart(cartItemId);
        return ResponseEntity.noContent().build();
    }

    // Endpoint to update an item in the cart
    @PutMapping("/update/{cartItemId}")
    public ResponseEntity<Void> updateCartItem(@PathVariable Long cartItemId, @RequestBody CartItem cartItem) {
        cartDao.updateCartItem(cartItemId, cartItem);
        return ResponseEntity.noContent().build();
    }
}
