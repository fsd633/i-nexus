package com.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDao;
import com.model.User;
import com.model.UserLogin;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    UserDao userDao;

    @GetMapping("getAllUsers")
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @GetMapping("getUserByEmail/{emailId}")
    public User getUserByEmail(@PathVariable("emailId") String emailId) {
        return userDao.getUserByEmail(emailId);
    }

    @GetMapping("getUserById/{uId}")
    public User getUserById(@PathVariable("uId") int uId) {
        return userDao.getUserById(uId);
    }

    @PostMapping("registerUser")
    public User registerUser(@RequestBody User user) {
        return userDao.registerUser(user);
    }

    @PostMapping("userLogin")
    public User userLogin(@RequestBody UserLogin userLogin) {
        return userDao.userLogin(userLogin.getEmailId(), userLogin.getPassword());
    }

    @PutMapping("updateUserPassword")
    public User updateUserPassword(@RequestBody User user) {
        return userDao.updateUserPassword(user);
    }
}