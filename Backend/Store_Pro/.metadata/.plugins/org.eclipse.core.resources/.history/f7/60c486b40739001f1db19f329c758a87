package com.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDao;
import com.model.User;
import com.model.UserLogin;
import com.service.EmailService;
import com.util.OtpUtil;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    UserDao userDao;

    @GetMapping("getAllUsers")
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @GetMapping("getUserByEmail/{emailId}")
    public User getUserByEmail(@PathVariable("emailId") String emailId) {
        return userDao.getUserByEmail(emailId);
    }

    @GetMapping("getUserById/{uId}")
    public User getUserById(@PathVariable("uId") int uId) {
        return userDao.getUserById(uId);
    }

    @PostMapping("registerUser")
    public User registerUser(@RequestBody User user) {
        return userDao.registerUser(user);
    }

    @PostMapping("userLogin")
    public User userLogin(@RequestBody UserLogin userLogin) {
        return userDao.userLogin(userLogin.getEmailId(), userLogin.getPassword());
    }

    @PutMapping("updateUserPassword")
    public User updateUserPassword(@RequestBody User user) {
        return userDao.updateUserPassword(user);
    }
    

	@PostMapping("/forgot-password")
	public ResponseEntity<Map<String, String>> forgotPassword(@RequestBody OtpRequest otpRequest) {
		userDao.sendOtp(otpRequest.getEmailId());
		Map<String, String> response = new HashMap<>();
		response.put("message", "OTP sent to email!");
		return ResponseEntity.ok(response);
	}
    
    @GetMapping("/otpToEmail")
    public ResponseEntity<String> sendOtp(@RequestParam String email) {
        System.out.println("Received email for OTP: " + email);
        User user = userDao.findByEmailId(email);

        if (user == null) {
            System.out.println("Email not registered: " + email);
            return ResponseEntity.badRequest().body("Email not registered");
        }

        String otp = OtpUtil.generateOtp();
        System.out.println("Generated OTP: " + otp);

        try {
            EmailService.sendMail("One Time Password (OTP): " + otp, email, null);
        } catch (Exception e) {
            System.out.println("Error in sending email");
            e.printStackTrace();
            return ResponseEntity.status(500).body("Error in sending email");
        }

        return ResponseEntity.ok("OTP sent successfully");
    }

}